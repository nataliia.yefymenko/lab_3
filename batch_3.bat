@echo off
chcp 65001
setlocal EnableDelayedExpansion

echo Дана програма рахує обсяг підкаталогів у заданій директорії
set /p "dirpath=Введіть шлях до директорії: "

if not exist "%dirpath%" (
    echo Дана директорія не знайдена!
    pause >nul
    exit /b
)

set /a total_size=0
for /f "delims=" %%d in ('dir /a:d /b /s "%dirpath%"') do (
    set "dir=%%d"
    set /a size=0
    for /f "tokens=3" %%i in ('dir /a /-c "%%d\*" ^| find "File(s)"') do (
        set /a size=%%i
    )
    set /a total_size+=size
    echo Обсяг підкаталогу "%%d": !size! байт
)
echo Загальний обсяг підкаталогів: %total_size% байт
echo Роботу програми завершено, для виходу натисніть будь яку клавішу!
pause >nul
